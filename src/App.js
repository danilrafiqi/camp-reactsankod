import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Navigasi from './components/Navbar';
import TombolJumbo from './components/Jumbotron';
import Kartu from './components/Kartu';
import { Row, Container } from 'reactstrap';
import Tombol from './components/Tombol';
import Mondok from './components/Mondok';
import MondokLagi from './components/MondokLagi';
import Sponsor from './components/Sponsor';


class App extends Component {
  state= {
    data:[
      {
      gambar :'https://lh3.googleusercontent.com/XLlyoc7OLR2dgFdnhKk-83-6_kxoTXu-6RyPcUiFc_UiSwZmuIvibNfS307Crb3IcFSbPjq1EdHNIknl-_4N5QAVpBFAZmGeC3d6ZMLwng5eAhnKBnoD1V-pcItiFukHZIq0UgA62NvO_Yg',
      judul:'Kajian 3',
      deskripsi: 'mumet',
      penulis:'danil',
      kuota:40
    },
    {
      gambar :'https://lh5.googleusercontent.com/-JajpG87BjXAxqMCAu7bs_Bb-c84GUdFtG9w0mNB_dQ1oaOnT3aRYYllYr9tHnB-rLJ8ZxpOHA=w2381',
      judul:'Kajian 3',
      deskripsi: 'mumet',
      penulis:'danil',
      kuota:40
    },
    {
      gambar :'https://lh5.googleusercontent.com/Nw8x_CE9s4N8WGggkhxit5nHdo8j03kr0daRbRTRhduOtJzwmaw3h-iR0T4iBiXrEyQqN-q_VA=w3572',
      judul:'Kajian 3',
      deskripsi: 'mumet',
      penulis:'danil',
      kuota:40
    }],
    tombol : [
      'Daftar Kajian Koding Rutin',
      'Daftar Santren Kilat Koding',
      'Lihat Selengkapnya'
    ],
    santrenKilat: [
    {
      gambar :'https://lh5.googleusercontent.com/e6ixy5iFrGuPwlZFn0oU0vPgqMT9zPKdmswiBbZJEVpSbH6L2gVlcKAKFAoz7n31T45ZsSx8tQ=w1191',
      judul:'Santren Kilat',
      deskripsi: 'halo dunia',
      penulis:'danil',
      kuota:20
    }]
  }
  render() {
    return (
      <div>
        <Navigasi/>
        <div className="App-body">
          <TombolJumbo/>
          <h2>Daftar Kajian Koding Rutin</h2>
          <Container>
            <Row>
              {
                this.state.data.map(
                  (angka, index) => {
                    return(
                      <Kartu
                        key={index}
                        gambar={angka.gambar}
                        judul={angka.judul}
                        deskripsi={angka.deskripsi}
                        penulis={angka.penulis}
                        kuota={angka.kuota}
                      />
                    )
                  }
                )
              }
            </Row>
            <br/>
            <Tombol tombol={this.state.tombol[0]}/>
          </Container>
          <hr/>

          <Container>
            <Row>
              {
                this.state.santrenKilat.map(
                  (angka, index) => {
                    return(
                      <Kartu
                        key={index}
                        gambar={angka.gambar}
                        judul={angka.judul}
                        deskripsi={angka.deskripsi}
                        penulis={angka.penulis}
                        kuota={angka.kuota}
                      />
                    )
                  }
                )
              }
            </Row>
            <br/>
            <Tombol tombol={this.state.tombol[1]}/>
          </Container>

          <hr/>
          <Mondok/>
          <hr/>
          <MondokLagi/>
          <br/>
          <Tombol tombol={this.state.tombol[2]}/>
          <hr/>
          <Sponsor/>
        </div>
      </div>
    );
  }
}

export default App;
