import React from 'react';
import { Jumbotron, Button, Container } from 'reactstrap';


const Example = (props) => {
  return (
    <Jumbotron fluid={true} className='Jumbotron'>
      <Container
        fluid={true}
      >
        <h1 className="display-3">Belajar Jadi Asyik!</h1>
        <p className="lead">Kegiatan Belajar Intensif Programming dan Qur'an bersama Santren Koding.</p>
        <br/>
        <p className="lead">
          <Button color="success" size='lg'>Mulai Belajar</Button>
        </p>
      </Container>
    </Jumbotron>
  );
};

export default Example;