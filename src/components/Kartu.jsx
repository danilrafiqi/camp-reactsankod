import React from 'react';
import {
  Card,
  CardBody,
  CardTitle,
  CardText,
  CardImg,
  Col,
  Row
} from 'reactstrap';

const Example = (props) => {
  return (
    <Col xs="12" sm="12" md="6" lg="4">
      <Card>
        <CardImg top width="100%" src={props.gambar} alt="Card image cap" />
        <CardBody align= 'left'>
          <CardTitle>{props.judul}</CardTitle>
          <CardText>{props.deskripsi}</CardText>
          <Row>
            <Col sm="7">
              <a href="" className="Small">{props.penulis}</a>
            </Col>
            <Col align="right" sm="5">
              <span className="Small-red">
                {props.kuota}
              </span>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </Col>
  );
};

export default Example;