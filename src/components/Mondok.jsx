import React from 'react';
import {
	Row,
	Col,
	Container,
} from 'reactstrap';

const Mondok = (props) =>{
	return(
		<Container>
			<Row>
				<Col xs='12' sm='12' md='6' lg='6' className='bg-hijau'>
					<br/>
					<h4>Kegiatan</h4>
					<br/>
					<div align='center'>
						<p>Koding</p>
						<p>Belajar Agama dan Al Qur'an</p>
						<p>Pendidikan Karakter</p>
					</div>
				</Col>
				<Col xs='12' sm='12' md='6' lg='6' className='bg-putih'>
					<br/>
					<img height="100px" src="http://i63.tinypic.com/oid9xu.png"/>
					<h4>"Memberi Manfaat Untuk Ummat"</h4>
					<br/>
					<p>-Santren Koding-</p>
					<br/>
				</Col>
			</Row>
		</Container>
	)
}

export default  Mondok;