import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  } from 'reactstrap';

export default class Example extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/">
            <img src="http://i63.tinypic.com/oid9xu.png" className="d-inline-block align-top" width='50px' alt=""/>
          </NavbarBrand>
          <NavbarBrand href="/">
            Santren Koding
          </NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="https://rafibanget.blogspot.com">Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="https://rafibanget.blogspot.com">Kajian Koding</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="https://rafibanget.blogspot.com">Santren Kilat</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="https://rafibanget.blogspot.com">Mondok</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="https://rafibanget.blogspot.com">Tentang Kami</NavLink>
              </NavItem>
              <NavItem>
                <a href="https://rafibanget.blogspot.com" className="btn btn-outline-primary">
                  Login/Signup
                </a>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}
