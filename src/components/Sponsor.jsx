import React from 'react';
import {
	Container,
	Row,
	Col,
} from 'reactstrap';

const Sponsor = (props) =>{
	return(
		<div className='Sponsor'>
			<br/>
			<h3>Sponsor</h3>
			<br/>
			<Row>
				<Col xs='12' sm='12' md='4' lg='4'>
				</Col>
				<Col xs='12' sm='12' md='4' lg='4'>
					<a href="http://klinikita.co.id/">
						<img width="80%" className="box-shadow" src="http://i67.tinypic.com/2hcn70k.jpg"/>
					</a>
				</Col>
				<Col xs='12' sm='12' md='4' lg='4'>
				</Col>
				<br/>
				<br/>
				<Container>
					<Row>
						<Col xs='6' sm='6' md='4' lg='2'>
							<a href="https://www.undip.ac.id/language/id/">
								<img height="100px" src="https://1.bp.blogspot.com/-GZjl-D1QU6k/WgpvD8krquI/AAAAAAAAE0Y/tkrHzHHdt-4BdvmofUT7vuNtjG4ANIRPQCLcBGAs/s400/Undip.png"/>
							</a>
						</Col>
						<Col xs='6' sm='6' md='4' lg='2'>
							<a href="http://www.dinus.ac.id/">
							<img height="100px" src="http://adiwibowo.files.wordpress.com/2012/10/logo-udinus.png?resize=370%2C358"/></a>
						</Col>
						<Col xs='6' sm='6' md='4' lg='2'>
							<a href="https://www.unisbank.ac.id/">
							<img height="100px" src="https://upload.wikimedia.org/wikipedia/id/archive/6/6a/20150926142003%21Logo_unisbank.jpg"/>
							</a>
						</Col>
						<Col xs='6' sm='6' md='4' lg='2'>
							<a href="http://www.java-valley.com/">
							<img height="100px" src="http://id.indonesiayp.com/img/id/c/1445918663-86-pt-java-valley-technology.png"/>
							</a>
						</Col>
						<Col xs='6' sm='6' md='4' lg='2'>
							<a href="https://www.dynamiclearningid.org/">
							<img height="100px" src="https://dynamiclearningid.files.wordpress.com/2017/01/orderdilla.png?w=500"/>
							</a>
						</Col>
						<Col xs='6' sm='6' md='4' lg='2'>
							<a href="https://www.go-mekanik.com/">
							<img height="100px" src="https://www.go-mekanik.com/assets/public/src/imgs/gomekanik/logo.png"/>
							</a>
						</Col>
					</Row>
					<br/>
					<Row>
						<Col xs='6' sm='6' md='4' lg='2'>
							<a href="https://www.undip.ac.id/language/id/">
								<img height="100px" src="http://sdk.semarangkota.go.id/web/pemkot.png"/>
							</a>
						</Col>
						<Col xs='6' sm='6' md='4' lg='4'>
							<a href="http://www.dinus.ac.id/">
							<img height="80px" src="http://www.sandec.org/images/sandec-logo.png"/></a>
						</Col>
						<Col xs='6' sm='6' md='4' lg='2'>
							<a href="https://www.unisbank.ac.id/">
							<img height="120px" src="http://i64.tinypic.com/28v94w6.png"/>
							</a>
						</Col>
						<Col xs='6' sm='6' md='4' lg='4'>
							<a href="http://www.java-valley.com/">
							<img height="50px" src="http://sdk.semarangkota.go.id/komunitas/logokomunitas/20161215114909logo-p.png"/>
							</a>
						</Col>
					</Row>
					<br/>
				</Container>
			</Row>
		</div>
	)
}

export default Sponsor;