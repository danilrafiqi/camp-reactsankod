import React from 'react';
import {
	Row,
	Col,
	Button
  } from 'reactstrap';

const Tombol = (props)=>{
	return(
		<Row>
			<Col>
				<a>
					<Button outline color='success'>
						{props.tombol}
					</Button>
				</a>
			</Col>
		</Row>
	);
};

export default  Tombol;